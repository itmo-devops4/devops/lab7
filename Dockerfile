FROM node as builder

WORKDIR /app
COPY ./app/package.json ./app/package-lock.json ./app/index.js ./
COPY ./app/test/  ./test/
RUN npm install --prod

FROM astefanutti/scratch-node
COPY --from=builder /app /
