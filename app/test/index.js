let server = require("../index")
let chai = require("chai")
let chaiHttp = require("chai-http")

chai.should()
chai.use(chaiHttp)

describe("index api", () => {
    it("return plain text", (done) => {
        chai.request(server)
            .get("/index")
            .end((err, response) => {
                response.should.have.status(200)
                done(err)
            })
    })
})
