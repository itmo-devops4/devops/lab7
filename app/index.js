const express = require('express')
  
const app = express()
const PORT = process.env.PORT || 3000

app.get('/index', (req, res)=>{
    res.status(200).send("Privet, chupapiki! Nikita Shestakov loh!");
})

const server = app.listen(PORT, (error) =>{
    if(!error)
        console.log("Server is Successfully Running, and App is listening on port " + PORT)
    else 
        console.log("Error occurred, server can't start on port" + PORT, error);
    }
)

module.exports = server
